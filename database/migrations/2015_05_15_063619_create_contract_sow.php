<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContractSow extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('contract_sow', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('contract_id');
			$table->mediumText('features');
			$table->mediumText('wireframe');
			$table->text('technology');
			$table->text('project_requirement');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('contract_sow');
	}

}
