@extends('backend._template.main')

@section('content')
<!-- Main content -->
<section class="content">
    <div class="row">
    	<!-- left column -->
    	<div class="col-md-4">
    	    <!-- general form elements -->
    	    <div class="box box-primary">
    	        <div class="box-header">
    	            <h3 class="box-title">Quick Example</h3>
    	        </div><!-- /.box-header -->
    	        <!-- form start -->
    	        <form role="form">
    	            <div class="box-body">
    	                <div class="form-group">
    	                    <label for="blogTitle">Title</label>
    	                    <input type="text" class="form-control" id="blogTitle" placeholder="Blog Title">
    	                </div>
    	                <div class="form-group">
    	                    <label for="blogAuthor">Author</label>
    	                    <input type="text" class="form-control" id="blogAuthor" placeholder="Author Name">
    	                </div>
    	            </div><!-- /.box-body -->

    	            <div class="box-footer">
    	                <button type="submit" class="btn btn-primary">Submit</button>
    	            </div>
    	        </form>
    	    </div><!-- /.box -->
    	</div>
    	<div class="col-md-8">
    		<div class="box box-primary">
    		    <div class="box-header">
    		        <h3 class="box-title">Content</h3>
    		    </div><!-- /.box-header -->
    		    <div class='box-body pad'>
    		        <form>
    		            <textarea class="text-editor" placeholder="Place some text here" style="width: 100%; height: 300px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
    		        </form>
    		    </div>

    		</div>
    	</div>
    </div>
</section>
@stop

@section('page-styles')
<link href="{{asset('assets/backend/css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')}}" rel="stylesheet" type="text/css" />
@stop

@section('page-scripts')
<script src="{{asset('assets/backend/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}" type="text/javascript"></script>
<script type="text/javascript">
	$(function(){
		$(".text-editor").wysihtml5();
	});
</script>
@stop

