<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 paceSimple sidebar sidebar-fusion"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 paceSimple sidebar sidebar-fusion"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 paceSimple sidebar sidebar-fusion"> <![endif]-->
<!--[if gt IE 8]> <html class="ie paceSimple sidebar sidebar-fusion"> <![endif]-->
<!--[if !IE]><!--><html class="paceSimple sidebar sidebar-fusion"><!-- <![endif]-->
<head>
    <title>{{env('APP_PROJECT_NAME') ." - {$page_title} ". env('APP_VERSION')}}</title>

    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">

   <!--[if lt IE 9]><link rel="stylesheet" href="{{asset('assets/frontend/library/bootstrap/css/bootstrap.min.css?v=1.0')}}" /><![endif]-->
   <link rel="stylesheet" href="{{asset('assets/frontend/less/pages/serve-style.css?v=1.0.1')}}" />
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <script type="text/javascript" src="{{asset('assets/frontend/plugins/core_ajaxify_loadscript/script.min62f6.js?v=v1.0')}}"></script>

    @yield('page-styles')

</head>

<body class="scripts-async">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden">
		<!-- Sidebar Menu -->
		<div id="menu" class="hidden-print hidden-xs">
			<div id="sidebar-fusion-wrapper">
			<!--<input class="form-control search" type="text" placeholder="Search...">-->
			<div class="search-wrapper">
		    	<div class="input-group">
		        	<input type="text" class="form-control" placeholder="Search">
		    		<span class="input-group-btn">
		        	<button class="btn btn-inverse" type="button">
		            	<i class="fa fa-search"></i>
		        	</button>
		    		</span>
		    	</div><!-- /input-group -->
			</div>

		    <div id="logoWrapper">
		        <div class="media">
		            <a href="#" class="pull-left">
		            	<img src="{{asset('assets/frontend/images/people/50/2.jpg')}}" alt="" class="img-circle" >
		            </a>
		            <div class="media-body">
		                <a href="#" class="name">Adrian D.</a>
		                <p><i class="fa fa-fw fa-circle-o text-success"></i> Online</p>
		            </div>
		            <div class="clearfix"></div>
		            <a href="usera10b.html?v=v1.0.0-rc1" class="btn btn-xs btn-inverse ">
		            	<i class="fa fa-user"></i>
		            </a>
		            <a href="user/projectsa10b.html?v=v1.0.0-rc1" class="btn btn-xs btn-inverse ">
		            	<i class="fa fa-clock-o"></i>
		            </a>
		            <a href="user/messagesa10b.html?v=v1.0.0-rc1" class="btn btn-xs btn-inverse ">
		            	<i class="fa fa-envelope"></i>
		            </a>
		        </div>
		        <select class="selectpicker" data-style="btn-inverse" id="select_project">
		            <option>Select Project</option>
		            <option value="project_milestones.html?v=v1.0.0-rc1">Project #1</option>
		            <option value="project_milestones.html?v=v1.0.0-rc1">Project #2</option>
		            <option value="project_milestones.html?v=v1.0.0-rc1">Project #3</option>
		        </select>
		    </div>

			<ul class="menu list-unstyled">
				<li class="{{Helper::is_active($routes['dashboard'],'active')}}">
					<a href="{{route('dashboard.index')}}" class="index">
		            	<i class="fa fa-home"></i><span>Overview</span>
		        	</a>
				</li>
				<li class="hasSubmenu {{Helper::is_active($routes['contracts'],'active')}}">
					<a href="#menu-contracts" data-toggle="collapse">
		                <i class="fa fa-lock"></i><span>Contracts</span>
					</a>
					<ul class="collapse {{Helper::is_active($routes['contracts'],'in')}}" id="menu-contracts">
		                <li class="{{Helper::is_active($routes['contracts'][0])}}">
							<a href="{{route('dashboard.contracts.index')}}" class="no-ajaxify">
		                		<i class="fa fa-lock"></i><span>All Record</span>
							</a>
						</li>
						<li class="{{Helper::is_active($routes['contracts'][1])}}">
							<a href="{{route('dashboard.contracts.create-moa')}}" class="no-ajaxify">
		                		<i class="fa fa-pencil"></i><span>Create New</span>
							</a>
						</li>
						<li class="{{Helper::is_active($routes['contracts'][2])}}">
							<a href="{{route('dashboard.contracts.create-annexa')}}" class="no-ajaxify">
		                		<i class="fa fa-pencil"></i><span>Create Annex A</span>
							</a>
						</li>
						<li class="{{Helper::is_active($routes['contracts'][3])}}">
							<a href="{{route('dashboard.contracts.create-annexb')}}" class="no-ajaxify">
		                		<i class="fa fa-pencil"></i><span>Create Annex B</span>
							</a>
						</li>
						</li>
						<li class="{{Helper::is_active($routes['contracts'][4])}}">
							<a href="{{route('dashboard.contracts.create-annexc')}}" class="no-ajaxify">
		                		<i class="fa fa-pencil"></i><span>Create Annex C</span>
							</a>
						</li>
				    </ul>
				</li>
				<li class="hasSubmenu">
					<a href="#menu-account-setting" data-toggle="collapse">
		                <i class="fa fa-lock"></i><span>Account Setting</span>
					</a>
					<ul class="collapse" id="menu-account-setting">
		                <li class="">
							<a href="logina10b.html?v=v1.0.0-rc1" class="no-ajaxify">
		                		<i class="fa fa-lock"></i><span>Profile</span>
							</a>
						</li>
						<li class="">
							<a href="signupa10b.html?v=v1.0.0-rc1" class="no-ajaxify">
		                		<i class="fa fa-pencil"></i><span>Credentials</span>
							</a>
						</li>
				    </ul>
				</li>
				{{-- <li class="">
					<a href="projects_grida10b.html?v=v1.0.0-rc1">
		                <i class="fa fa-th-large"></i><span>Projects Grid</span>
					</a>
				</li>
				<li class="">
					<a href="projects_lista10b.html?v=v1.0.0-rc1">
		                <i class="fa fa-list"></i><span>Projects List</span>
					</a>
				</li>
				<li class="">
					<a href="project_milestonesa10b.html?v=v1.0.0-rc1">
		                <i class="fa fa-clock-o"></i><span>Project Overview</span>
		            </a>
				</li>
				<li class="">
					<a href="project_tasksa10b.html?v=v1.0.0-rc1">
		                <i class="fa fa-ticket"></i><span>Tasks</span>
					</a>
				</li>
				<li class="">
					<a href="usera10b.html?v=v1.0.0-rc1">
		                <i class="fa fa-user"></i><span>User Account</span>
					</a>
				</li>
				<li class="">
					<a href="calendara10b.html?v=v1.0.0-rc1" class="calendar">
		                <i class="fa fa-calendar"></i><span>Calendar</span>
					</a>
				</li>
				<li class="hasSubmenu">
					<a href="#menu-ee7de0dbbde2c42f5211097eec39e7f0" data-toggle="collapse">
		                <i class="fa fa-lock"></i><span>Access</span>
					</a>
					<ul class="collapse" id="menu-ee7de0dbbde2c42f5211097eec39e7f0">
		                <li class="">
							<a href="logina10b.html?v=v1.0.0-rc1" class="no-ajaxify">
		                		<i class="fa fa-lock"></i><span>Login</span>
							</a>
						</li>
						<li class="">
							<a href="signupa10b.html?v=v1.0.0-rc1" class="no-ajaxify">
		                		<i class="fa fa-pencil"></i><span>Sign up</span>
							</a>
						</li>
				    </ul>
				</li>
				<li class="">
					<a href="pricing_tablesa10b.html?v=v1.0.0-rc1" class="pricing">
		                <i class="fa fa-money"></i><span>Pricing</span>
					</a>
				</li>
				<li class="">
					<a href="support_forum_overviewa10b.html?v=v1.0.0-rc1" class="forum">
		                <i class="fa fa-life-ring"></i><span>Forum</span>
					</a>
				</li>
				<li class="">
					<a href="support_kba10b.html?v=v1.0.0-rc1">
		                <i class="fa fa-file-text-o"></i><span>Knowledge Base</span>
					</a>
				</li>
				<li class="hasSubmenu">
					<a href="#menu-df53cc9811acb7874f2893663be59ce7" data-toggle="collapse">
		                <i class="fa fa-th-large"></i><span>Components</span>
					</a>
					<ul class="collapse" id="menu-df53cc9811acb7874f2893663be59ce7">
		                <li class="">
							<a href="uia10b.html?v=v1.0.0-rc1">
		                		<i class="fa fa-circle-o"></i><span>UI Elements</span>
							</a>
						</li>
						<li class="">
							<a href="iconsa10b.html?v=v1.0.0-rc1">
		                		<i class="fa fa-circle-o"></i><span>Icons</span>
							</a>
						</li>
						<li class="">
							<a href="typographya10b.html?v=v1.0.0-rc1">
		                		<i class="fa fa-circle-o"></i><span>Typography</span>
							</a>
						</li>
						<li class="">
							<a href="tabsa10b.html?v=v1.0.0-rc1">
		                		<i class="fa fa-circle-o"></i><span>Tabs</span>
							</a>
						</li>
						<li class="hasSubmenu">
							<a href="#menu-ccc6db02c7ca68690dfedeecbd032557" data-toggle="collapse">
		                		<span class="badge pull-right badge-primary">3</span>
		                		<i class="fa fa-circle-o"></i><span>Tables</span>
		                	</a>
					        <ul class="collapse" id="menu-ccc6db02c7ca68690dfedeecbd032557">
		                    	<li class="">
									<a href="tablesa10b.html?v=v1.0.0-rc1">
		                				<span>Tables</span>
									</a>
								</li>
								<li class="">
					                <a href="tables_responsivea10b.html?v=v1.0.0-rc1">
		                                <span>Responsive Tables</span>
									</a>
								</li>
				            </ul>
						</li>
						<li class="hasSubmenu">
							<a href="#menu-dc2fbe7c28f5199e085dd21d4d3216dc" data-toggle="collapse">
		                		<span class="badge pull-right badge-primary">4</span>
		                		<i class="fa fa-circle-o"></i> <span>Forms</span>
							</a>
					        <ul class="collapse" id="menu-dc2fbe7c28f5199e085dd21d4d3216dc">
		                    	<li class="">
									<a href="form_wizardsa10b.html?v=v1.0.0-rc1">
		                				<span>Form Wizards</span>
									</a>
								</li>
								<li class="">
					                <a href="form_elementsa10b.html?v=v1.0.0-rc1">
		                				<span>Form Elements</span>
									</a>
								</li>
								<li class="">
					                <a href="form_validatora10b.html?v=v1.0.0-rc1">
		                            <span>Form Validator</span>
								</a>
								</li>
								<li class="">
									<a href="file_managersa10b.html?v=v1.0.0-rc1">
			                    		<span>File Managers</span>
			                    	</a>
								</li>
				        	</ul>
		                </li>
						<li class="">
							<a href="slidersa10b.html?v=v1.0.0-rc1">
		                		<i class="fa fa-circle-o"></i><span>Sliders</span>
							</a>
						</li>
						<li class="">
							<a href="chartsa10b.html?v=v1.0.0-rc1">
		                		<i class="fa fa-circle-o"></i><span>Charts</span>
							</a>
						</li>
						<li class="">
							<a href="grida10b.html?v=v1.0.0-rc1">
		                		<i class="fa fa-circle-o"></i><span>Grid</span>
							</a>
						</li>
						<li class="">
							<a href="notificationsa10b.html?v=v1.0.0-rc1">
		                		<i class="fa fa-circle-o"></i><span>Notifications</span>
							</a>
						</li>
						<li class="">
							<a href="modalsa10b.html?v=v1.0.0-rc1">
		                		<i class="fa fa-circle-o"></i><span>Modals</span>
							</a>
						</li>
						<li class="">
							<a href="thumbnailsa10b.html?v=v1.0.0-rc1">
		                		<i class="fa fa-circle-o"></i><span>Thumbnails</span>
							</a>
						</li>
						<li class="">
							<a href="carouselsa10b.html?v=v1.0.0-rc1">
		                		<i class="fa fa-circle-o"></i><span>Carousels</span>
							</a>
						</li>
						<li class="">
							<a href="image_cropa10b.html?v=v1.0.0-rc1">
		                		<i class="fa fa-circle-o"></i><span>Image Cropping</span>
							</a>
						</li>
						<li class="">
							<a href="twittera10b.html?v=v1.0.0-rc1">
		                		<i class="fa fa-circle-o"></i> <span>Twitter API</span>
							</a>
						</li>
						<li class="">
							<a href="infinite_scrolla10b.html?v=v1.0.0-rc1">
		                		<i class="fa fa-circle-o"></i><span>Infinite Scroll</span>
							</a>
						</li>
				    </ul>
				</li> --}}
			</ul>
		</div>
		</div>
		<!-- Content -->
		<div id="content">
			<div class="navbar hidden-print main navbar-default" role="navigation">
				<div class="user-action user-action-btn-navbar pull-right">
					<button class="btn btn-sm btn-navbar btn-inverse btn-stroke hidden-lg hidden-md">
						<i class="fa fa-bars fa-2x"></i>
					</button>
				</div>
				<a href="index43fd.html" class="logo">
					<img src="{{asset('assets/frontend/images/logo/logo.jpg')}}" width="32" alt="SMART" />
					<span class="hidden-xs hidden-sm inline-block"><span>mosaic</span>pro</span>
				</a>
				
				<ul class="main pull-left hidden-xs ">
					<li class="dropdown notif notifications hidden-xs">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<i class="fa fa-envelope"></i><span class="badge badge-danger">5</span>
						</a>
						<ul class="dropdown-menu chat media-list">
							<li class="media">
				        		<a class="pull-left" href="#">
				        			<img class="media-object thumb" src="../assets/images/people/100/15.jpg" alt="50x50" width="50" />
				        		</a>
								<div class="media-body">
				        			<span class="label label-default pull-right">5 min</span>
				            		<h5 class="media-heading hidden-xs">Adrian D.</h5>
				           			 <p class="margin-none">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
				       			 </div>
							</li>
					      	<li class="media">
					          	<a class="pull-left" href="#">
					          		<img class="media-object thumb" src="../assets/images/people/100/16.jpg" alt="50x50" width="50" />
					          	</a>
								<div class="media-body">
						          	<span class="label label-default pull-right">2 days</span>
						            <h5 class="media-heading">Jane B.</h5>
						            <p class="margin-none">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
						        </div>
					        </li>
				    		<li class="media">
			          			<a class="pull-left" href="#">
			          				<img class="media-object thumb" src="../assets/images/people/100/17.jpg" alt="50x50" width="50" />
			          			</a>
				      			<div class="media-body">
									<span class="label label-default pull-right">3 days</span>
				            		<h5 class="media-heading">Andrew M.</h5>
				            		<p class="margin-none">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
				        		</div>
			        		</li>
			        		<li>
			        			<a href="#" class="btn btn-primary">
			        				<i class="fa fa-list"></i> <span>View all messages</span>
			        			</a>
			        		</li>
		      			</ul>
					</li>
					<li class="notifications">
						<a href="user/projectsa10b.html?v=v1.0.0-rc1">
							<i class="fa fa-clock-o"></i> <span class="badge badge-warning">7</span>
						</a>
					</li>
					<li class="notifications">
						<a href="usera10b.html?v=v1.0.0-rc1" >
							<i class="fa fa-user"></i> <span class="badge badge-info">2</span>
						</a>
					</li>
				</ul>
				<ul class="main pull-right">
					<li class="hidden-xs hidden-sm">
						<a href="#modal-task" class="btn btn-info" data-toggle="modal">
							Create Task  <i class="fa fa-fw icon-compose"></i>
						</a>
					</li>
					<li class="hidden-xs">
						Completed Tasks:  <strong class="text-primary">24</strong>
					</li>
					<li class="dropdown username hidden-xs ">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<img src="../assets/images/people/35/22.jpg" class="img-circle" alt="Profile" /> Adrian D. <span class="caret"></span>
						</a>

						<ul class="dropdown-menu pull-right">
							<li>
								<a href="usera10b.html?v=v1.0.0-rc1" class="glyphicons user"><i></i> Account</a>
							</li>
							<li>
								<a href="user/messagesa10b.html?v=v1.0.0-rc1" class="glyphicons envelope"><i></i>Messages</a>
							</li>
							<li>
								<a href="user/projectsa10b.html?v=v1.0.0-rc1" class="glyphicons settings"><i></i>Projects</a>
							</li>
							<li>
								<a href="logina10b.html?v=v1.0.0-rc1" class="glyphicons lock no-ajaxify"><i></i>Logout</a>
							</li>
			    		</ul>
					</li>
				</ul> 
			</div>


		{{-- <div class="top-content-menu">
		    <a href="#" class="visible-xs " data-toggle="collapse" data-target="#top-menu">
		    	Submenu <i class="fa fa-caret-down pull-right"></i>
		    </a>
			<div class="collapse in" id="top-menu">
				<ul class="list-unstyled " >
					<li class="active">
						<a href="indexa10b.html?v=v1.0.0-rc1" >Home</a>
					</li>
					<li class="">
						<a href="uia10b.html?v=v1.0.0-rc1" >User Interface</a>
					</li>
					<li class="">
						<a href="form_elementsa10b.html?v=v1.0.0-rc1" >Form Elements</a>
					</li>
					<li class="">
						<a href="tablesa10b.html?v=v1.0.0-rc1" >Tables</a>
					</li>
					<li class="">
						<a href="maps_googlea10b.html?v=v1.0.0-rc1" >Maps</a>
					</li>
					<li class="">
						<a href="typographya10b.html?v=v1.0.0-rc1">Typography</a>
					</li>
				</ul>		
			 </div>
		</div> --}}

		@yield('content')

{{-- <div class="pull-right alert-wrapper hidden-xs">
	<div class="alert alert-inverse fade in">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="icon-delete-symbol"></i></button>
      <span class="title pull-left"><i class="icon-time-clock text-muted"> </i>  Alert!</span>
      <div class="clearfix"></div>
      <p class="content">A new ticket has been assigned <span class="text-warning">#9892</span></p>
  	</div>
</div> --}}

		
		</div>
		<!-- // Content END -->
		
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		
		<div id="footer" class="hidden-print">
			<!--  Copyright Line -->
			<div class="copy">&copy;  2015 <a href="#">Highly Succeed Inc.</a> Project Manager</div>
			<!--  End Copyright Line -->
		</div>
		
		<!-- // Footer END -->
		
	</div>
	<!-- // Main Container Fluid END -->
	

    <script data-id="App.Config">
        var basePath = '',
            commonPath = "{{URL::to('assets/frontend')}}",
            rootPath = "{{URL::to('/')}}",
            DEV = false,
            componentsPath = "{{URL::to('assets/frontend/components')}}";

		var primaryColor = '#c72a25',
            dangerColor = '#b55151',
            successColor = '#609450',
            infoColor = '#4a8bc2',
            warningColor = '#ab7a4b',
            inverseColor = '#45484d';

		var themerPrimaryColor = primaryColor;

        /*App.Config = {
    		ajaxify_menu_selectors: ['#menu'],
    		ajaxify_layout_app: false };*/
    </script>

    <script>var App = {};</script>

    <script data-id="App.Scripts">
    App.Scripts = {

    	/* CORE scripts always load first; */
    	

    	/* PLUGINS_DEPENDENCY always load after CORE but before PLUGINS; */
    	plugins_dependency: [
    		"{{asset('assets/frontend/library/bootstrap/js/bootstrap.min.js?v=v1.0')}}", 
    		"{{asset('assets/frontend/library/jquery/jquery-migrate.min.js?v=v1.0.0')}}", 
    		"{{asset('assets/frontend/plugins/charts_flot/jquery.flot62f6.js?v=v1.0')}}"
    	],

    	/* PLUGINS always load after CORE and PLUGINS_DEPENDENCY, but before the BUNDLE / initialization scripts; */
    	plugins: [
    		"{{asset('assets/frontend/plugins/core_ajaxify_davis/davis.min.js?v=v1.0')}}", 
    		"{{asset('assets/frontend/plugins/core_ajaxify_lazyjaxdavis/jquery.lazyjaxdavis.min62f6.js?v=v1.0')}}",
    		"{{asset('assets/frontend/plugins/core_preload/pace.min62f6.js?v=v1.0')}}", 
    		"{{asset('assets/frontend/plugins/core_nicescroll/jquery.nicescroll.min62f6.js?v=v1.0')}}", 
    		"{{asset('assets/frontend/plugins/core_breakpoints/breakpoints62f6.js?v=v1.0')}}", 
    		"{{asset('assets/frontend/plugins/charts_flot/jquery.flot.resize62f6.js?v=v1.0')}}", 
    		"{{asset('assets/frontend/plugins/charts_flot/plugins/jquery.flot.tooltip.min62f6.js?v=v1.0')}}", 
    		"{{asset('assets/frontend/components/charts_flot/flotcharts.common62f6.js?v=v1.0')}}" ,
    		"{{asset('assets/frontend/plugins/maps_vector/jquery-jvectormap-1.2.2.min62f6.js?v=v1.0')}}",
    		"{{asset('assets/frontend/plugins/maps_vector/data/gdp-dataa10b.js?v=v1.0')}}",
    		"{{asset('assets/frontend/plugins/maps_vector/maps/jquery-jvectormap-world-mill-en62f6.js?v=v1.0')}}", 
    		"{{asset('assets/frontend/plugins/maps_vector/maps/jquery-jvectormap-us-aea-en62f6.js?v=v1.0')}}", 
    		"{{asset('assets/frontend/plugins/maps_vector/maps/jquery-jvectormap-de-merc-en62f6.js?v=v1.0')}}", 
    		"{{asset('assets/frontend/plugins/maps_vector/maps/jquery-jvectormap-fr-merc-en62f6.js?v=v1.0')}}", 
    		"{{asset('assets/frontend/plugins/maps_vector/maps/jquery-jvectormap-es-merc-en62f6.js?v=v1.0')}}", 
    		"{{asset('assets/frontend/plugins/maps_vector/maps/jquery-jvectormap-us-lcc-en62f6.js?v=v1.0')}}", 
    		"{{asset('assets/frontend/plugins/maps_vector/maps/mall-map62f6.js?v=v1.0')}}", 
    		"{{asset('assets/frontend/plugins/forms_elements_bootstrap-select/js/bootstrap-select62f6.js?v=v1.0')}}", 
    		"{{asset('assets/frontend/plugins/forms_elements_bootstrap-datepicker/js/bootstrap-datepicker62f6.js?v=v1.0')}}", 
    		"{{asset('assets/frontend/plugins/ui_modals/bootbox.min62f6.js?v=v1.0')}}", 
    		"{{asset('assets/frontend/plugins/admin_notifications_gritter/js/jquery.gritter.mina10b.js?v=v1.0')}}", 
    		"{{asset('assets/frontend/plugins/core_less-js/less.min.js?v=v1.0')}}", 
    		"{{asset('assets/frontend/plugins/charts_flot/excanvas.js?v=v1.0')}}", 
    		"{{asset('assets/frontend/plugins/core_browser/ie/ie.prototype.polyfill.js?v=v1.0')}}",
    		"{{asset('assets/frontend/plugins/form_editor/js/summernote.min.js')}}",
    		"{{asset('assets/frontend/plugins/tagsinput/js/bootstrap-tagsinput.min.js')}}",

    	],

    	/* The initialization scripts always load last and are automatically and dynamically loaded when AJAX navigation is enabled; */
    	bundle: [
    		"{{asset('assets/frontend/components/core_ajaxify/ajaxify.init.js?v=v1.0')}}", 
    		"{{asset('assets/frontend/components/core_preload/preload.pace.init62f6.js?v=v1.0')}}", 
    		"{{asset('assets/frontend/components/charts_flot/flotchart-mixed-1.inita10b.js?v=v1.0')}}", 
    		"{{asset('assets/frontend/components/forms_elements_fuelux-checkbox/fuelux-checkbox.init62f6.js?v=v1.0')}}", 
    		"{{asset('assets/frontend/components/maps_vector/maps-vector.france-elections.init62f6.js?v=v1.0')}}", 
    		"{{asset('assets/frontend/components/maps_vector/maps-vector.mall-map.init62f6.js?v=v1.0')}}", 
    		"{{asset('assets/frontend/components/maps_vector/maps-vector.projection-map.init62f6.js?v=v1.0')}}", 
    		"{{asset('assets/frontend/components/maps_vector/maps-vector.random-colors.init62f6.js?v=v1.0')}}", 
    		"{{asset('assets/frontend/components/maps_vector/maps-vector.region-selection.init62f6.js?v=v1.0')}}", 
    		"{{asset('assets/frontend/components/maps_vector/maps-vector.usa-unemployment.init62f6.js?v=v1.0')}}", 
    		"{{asset('assets/frontend/components/maps_vector/maps-vector.world-map-gdp.init62f6.js?v=v1.0')}}", 
    		"{{asset('assets/frontend/components/maps_vector/maps-vector.world-map-markers.init62f6.js?v=v1.0')}}", 
    		"{{asset('assets/frontend/components/menus/sidebar.main.inita10b.js?v=v1.0')}}", 
    		"{{asset('assets/frontend/components/menus/sidebar.collapse.inita10b.js?v=v1.0')}}", 
    		"{{asset('assets/frontend/components/forms_elements_bootstrap-select/bootstrap-select.init62f6.js?v=v1.0')}}", 
    		"{{asset('assets/frontend/components/forms_elements_bootstrap-datepicker/bootstrap-datepicker.init62f6.js?v=v1.0')}}", 
    		"{{asset('assets/frontend/components/ui_modals/modals.init62f6.js?v=v1.0')}}", 
    		"{{asset('assets/frontend/components/admin_notifications_gritter/gritter.init62f6.js?v=v1.0')}}", 
    		"{{asset('assets/frontend/components/core/core.inita10b.js?v=v1.0')}}", 
    		"{{asset('assets/frontend/components/maps_vector/maps-vector.tabs62f6.js?v=v1.0')}}"
    	],
    	core: [
    		"{{asset('assets/frontend/library/modernizr/modernizr.js?v=v1.0')}}",
    		"{{asset('assets/frontend/library/jquery/jquery.min.js?v=v1.0.0')}}", 
    	]

    };
    </script>

    <script>
   
    $script(App.Scripts.core, 'core');
    $script.ready(['core'], function(){
    	$script(App.Scripts.plugins_dependency, 'plugins_dependency');
    });
    $script.ready(['core', 'plugins_dependency'], function(){
    	$script(App.Scripts.plugins, 'plugins');
    });
    $script.ready(['plugins','plugins_dependency','core'], function(){
    	$script(App.Scripts.bundle, 'bundle');
    });
    
    </script>
    <script>if (/*@cc_on!@*/false && document.documentMode === 10) { document.documentElement.className+=' ie ie10'; }</script>

    @yield('page-scripts')
    
</body>
</html>