<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 paceSimple"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 paceSimple"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 paceSimple"> <![endif]-->
<!--[if gt IE 8]> <html class="ie paceSimple"> <![endif]-->
<!--[if !IE]><!--><html class="paceSimple"><!-- <![endif]-->

<head>
    <title>{{env('APP_PROJECT_NAME') ." - {$page_title} ". env('APP_VERSION')}}</title>

    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">

    <!--[if lt IE 9]><link rel="stylesheet" href="{{asset('assets/frontend/library/bootstrap/css/bootstrap.min.css?v=1.0')}}" /><![endif]-->
    <link rel="stylesheet" href="{{asset('assets/frontend/less/pages/serve-style.css?v=1.0')}}" />
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <script type="text/javascript" src="{{asset('assets/frontend/library/jquery/jquery.min.js?v=v1.0.0')}}"></script>
    <script type="text/javascript" src="{{asset('assets/frontend/library/jquery/jquery-migrate.min.js?v=v1.0.0')}}"></script>
    <script type="text/javascript" src="{{asset('assets/frontend/library/modernizr/modernizr.js?v=v1.0')}}"></script>
    <script type="text/javascript" src="{{asset('assets/frontend/plugins/core_less-js/less.min.js?v=v1.0')}}"></script>
    <script type="text/javascript" src="{{asset('assets/frontend/plugins/charts_flot/excanvas.js?v=v1.0')}}"></script>
    <script type="text/javascript" src="{{asset('assets/frontend/plugins/core_browser/ie/ie.prototype.polyfill.js?v=v1.0')}}"></script>

    <script>if (/*@cc_on!@*/false && document.documentMode === 10) { document.documentElement.className+=' ie ie10'; }</script>

</head>
<body class="loginWrapper">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden">
		<!-- Content -->
		<div id="content">
			{{-- <div class="navbar hidden-print main navbar-default" role="navigation">
            	<div class="user-action user-action-btn-navbar pull-right">
            	   <button class="btn btn-sm btn-navbar btn-inverse btn-stroke hidden-lg hidden-md">
                    <i class="fa fa-bars fa-2x"></i>
                    </button>
            	</div>
        	<a href="index43fd.html" class="logo">
        		<img src="../assets/images/logo/logo.jpg" width="32" alt="SMART" />
        		<span class="hidden-xs hidden-sm inline-block"><span>mosaic</span>pro</span>
        	</a>
        	<ul class="main pull-left hidden-xs ">
        		<li class="dropdown notif notifications hidden-xs">
        			<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope"></i> <span class="badge badge-danger">5</span></a>
        			<ul class="dropdown-menu chat media-list">
        				<li class="media">
        			        <a class="pull-left" href="#"><img class="media-object thumb" src="../assets/images/people/100/15.jpg" alt="50x50" width="50" /></a>
        					<div class="media-body">
        			        	<span class="label label-default pull-right">5 min</span>
        			            <h5 class="media-heading hidden-xs">Adrian D.</h5>
        			            <p class="margin-none">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
        			        </div>
        				</li>
        		      	<li class="media">
        		          	<a class="pull-left" href="#"><img class="media-object thumb" src="../assets/images/people/100/16.jpg" alt="50x50" width="50" /></a>
        					<div class="media-body">
        			          	<span class="label label-default pull-right">2 days</span>
        			            <h5 class="media-heading">Jane B.</h5>
        			            <p class="margin-none">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
        			        </div>
        		        </li>
        			    <li class="media">
        		          	<a class="pull-left" href="#"><img class="media-object thumb" src="../assets/images/people/100/17.jpg" alt="50x50" width="50" /></a>
        			      	<div class="media-body">
        						<span class="label label-default pull-right">3 days</span>
        			            <h5 class="media-heading">Andrew M.</h5>
        			            <p class="margin-none">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
        			        </div>
        		        </li>
        		        <li><a href="#" class="btn btn-primary"><i class="fa fa-list"></i> <span>View all messages</span></a></li>
        	      	</ul>
        		</li>
        		<li class="notifications">
        			<a href="user/projects3ad4.html?v=v1.0.0-rc1&amp;skin=burnt-sienna"><i class="fa fa-clock-o"></i> <span class="badge badge-warning">7</span></a>
        		</li>
        		<li class="notifications">
        			<a href="user3ad4.html?v=v1.0.0-rc1&amp;skin=burnt-sienna" ><i class="fa fa-user"></i> <span class="badge badge-info">2</span></a>
        		</li>
        	</ul>
        	<ul class="main pull-right">
        		<li class="hidden-xs hidden-sm"><a href="#modal-task" class="btn btn-info" data-toggle="modal">Create Task  <i class="fa fa-fw icon-compose"></i></a></li>
        		<li class="hidden-xs">Completed Tasks:  <strong class="text-primary">24</strong></li>
        		<li class="dropdown username hidden-xs ">
        			<a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="../assets/images/people/35/22.jpg" class="img-circle" alt="Profile" /> Adrian D. <span class="caret"></span></a>

        			<ul class="dropdown-menu pull-right">
        				<li><a href="user3ad4.html?v=v1.0.0-rc1&amp;skin=burnt-sienna" class="glyphicons user"><i></i> Account</a></li>
        				<li><a href="user/messages3ad4.html?v=v1.0.0-rc1&amp;skin=burnt-sienna" class="glyphicons envelope"><i></i>Messages</a></li>
        				<li><a href="user/projects3ad4.html?v=v1.0.0-rc1&amp;skin=burnt-sienna" class="glyphicons settings"><i></i>Projects</a></li>
        				<li><a href="login3ad4.html?v=v1.0.0-rc1&amp;skin=burnt-sienna" class="glyphicons lock no-ajaxify"><i></i>Logout</a></li>
        		    </ul>
        		</li>
        	</ul> 
            </div> --}}

        @yield('content')    
        @yield('modal')
		</div>
		<!-- // Content END -->
		
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		
		<div id="footer" class="hidden-print">
			<!--  Copyright Line -->
			<div class="copy">&copy;  2015 <a href="#">Highly Succeed Inc.</a> Project Manager</div>
			<!--  End Copyright Line -->
		</div>
		
		<!-- // Footer END -->
		
	</div>
	<!-- // Main Container Fluid END -->
	

    <!-- Global -->
    <script data-id="App.Config">
        var App = {};        
        var basePath = '',
            commonPath = "{{URL::to('assets/frontend')}}",
            rootPath = "{{URL::to('/')}}",
            DEV = false,
            componentsPath = "{{URL::to('assets/frontend/components')}}";

        var primaryColor = '#c86f61',
            dangerColor = '#b55151',
            successColor = '#609450',
            infoColor = '#4a8bc2',
            warningColor = '#ab7a4b',
            inverseColor = '#45484d';

        var themerPrimaryColor = primaryColor;

    </script>
    <script type="text/javascript" src="{{asset('assets/frontend/library/bootstrap/js/bootstrap.min.js?v=v1.0')}}"></script>
    <script type="text/javascript" src="{{asset('assets/frontend/plugins/core_nicescroll/jquery.nicescroll.min62f6.js?v=v1.0')}}"></script>
    <script type="text/javascript" src="{{asset('assets/frontend/plugins/core_breakpoints/breakpoints62f6.js?v=v1.0')}}"></script>
    <script type="text/javascript" src="{{asset('assets/frontend/plugins/core_preload/pace.min62f6.js?v=v1.0')}}"></script>
    <script type="text/javascript" src="{{asset('assets/frontend/components/core_preload/preload.pace.init62f6.js?v=v1.0')}}"></script>
    <script type="text/javascript" src="{{asset('assets/frontend/plugins/forms_elements_bootstrap-datepicker/js/bootstrap-datepicker62f6.js?v=v1.0')}}"></script>
    <script type="text/javascript" src="{{asset('assets/frontend/components/forms_elements_bootstrap-datepicker/bootstrap-datepicker.init62f6.js?v=v1.0')}}"></script>
    <script type="text/javascript" src="{{asset('assets/frontend/plugins/ui_modals/bootbox.min62f6.js?v=v1.0')}}"></script>
    <script type="text/javascript" src="{{asset('assets/frontend/components/ui_modals/modals.init62f6.js?v=v1.0')}}"></script>
    <script type="text/javascript" src="{{asset('assets/frontend/plugins/admin_notifications_gritter/js/jquery.gritter.mina10b.js?v=v1.0')}}"></script>
    <script type="text/javascript" src="{{asset('assets/frontend/components/admin_notifications_gritter/gritter.init62f6.js?v=v1.0')}}"></script>
    <script type="text/javascript" src="{{asset('assets/frontend/components/core/core.inita10b.js?v=v1.0')}}"></script>
    <script type="text/javascript" src="{{asset('assets/frontend/components/forms_elements_fuelux-checkbox/fuelux-checkbox.init62f6.js?v=v1.0')}}"></script>
</body>

</html>