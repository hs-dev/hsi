@extends('frontend._template.main')

@section('content')
<h1 class="pull-left">
	Create Contract - MOA
</h1>
<div class="clearfix"></div>
<div class="innerLR">
	@include('frontend.page-notification')
	<form action="" method="POST">
		{!!Form::token()!!}
		<div class="row">
			<div class="col-md-6">
				<!-- Widget -->
				<div class="widget widget-heading-simple widget-body-white">
					<!-- Widget heading -->
					<div class="widget-head">
						<h4 class="heading">Project Name</h4>
					</div>
					<!-- // Widget heading END -->
					<div class="widget-body">
						<div class="form-group {{$errors->has('project_name')? 'has-error' : NULL}}">
							<input type="text" placeholder="Text input"  name="project_name" class="form-control"  value="{{old('project_name')}}" />
							@if($errors->has('project_name'))
                               <small class="text-danger">{{$errors->first('project_name')}}</small>
                            @endif
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<!-- Widget -->
				<div class="widget widget-heading-simple widget-body-white">
					<!-- Widget heading -->
					<div class="widget-head">
						<h4 class="heading">Project Type</h4>
					</div>
					<!-- // Widget heading END -->
					
					<div class="widget-body">
						<div class="form-group {{$errors->has('project_type')? 'has-error' : NULL}}">
							<select name="project_type" id="" class="form-control">
								<option value="">Select Type</option>
								<option value="Webapp" {{old('project_type')=='Webapp'? 'selected="Selected"' : NULL}}>Webapp</option>
								<option value="Logo" {{old('project_type')=='Logo'? 'selected="Selected"' : NULL}}>Logo</option>
								<option value="E-commerce" {{old('project_type')=='E-commerce'? 'selected="Selected"' : NULL}}>E-commerce</option>
							</select>

							@if($errors->has('project_type'))
                                <small>{{$errors->first('project_type')}}</small>
                            @endif
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<!-- Widget -->
				<div class="widget widget-heading-simple widget-body-white">
					<!-- Widget heading -->
					<div class="widget-head">
						<h4 class="heading">Client Name</h4>
					</div>
					<!-- // Widget heading END -->
					
					<div class="widget-body">
						<div class="form-group {{$errors->has('client_name')? 'has-error' : NULL}}">
							<input type="text" name="client_name" placeholder="Text input" class="form-control" value="{{old('client_name')}}">
							@if($errors->has('client_name'))
                                <small>{{$errors->first('client_name')}}</small>
                            @endif
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<!-- Widget -->
				<div class="widget widget-heading-simple widget-body-white">
					<!-- Widget heading -->
					<div class="widget-head">
						<h4 class="heading">Client Representative</h4>
					</div>
					<!-- // Widget heading END -->
					<div class="widget-body">
						<div class="form-group {{$errors->has('client_representative')? 'has-error' : NULL}}">
							<input type="text" name="client_representative" placeholder="Text input" class="form-control" value="{{old('client_representative')}}">
							@if($errors->has('client_representative'))
	                            <small>{{$errors->first('client_representative')}}</small>
	                        @endif
	                    </div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<!-- Widget -->
				<div class="widget widget-heading-simple widget-body-white">
					<!-- Widget heading -->
					<div class="widget-head">
						<h4 class="heading">Client Representative Position</h4>
					</div>
					<!-- // Widget heading END -->
					
					<div class="widget-body">
						<div class="form-group {{$errors->has('client_representative_position')? 'has-error' : NULL}}">
							<input type="text" name="client_representative_position" placeholder="Text input" class="form-control" value="{{old('client_representative_position')}}">
							@if($errors->has('client_representative_position'))
	                            <small>{{$errors->first('client_representative_position')}}</small>
	                        @endif
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<!-- Widget -->
				<div class="widget widget-heading-simple widget-body-white">
					<!-- Widget heading -->
					<div class="widget-head">
						<h4 class="heading">Signature Date</h4>
					</div>
					<!-- // Widget heading END -->
					
					<div class="widget-body">
						<div class="form-group {{$errors->has('signature_date')? 'has-error' : NULL}}">
							<input type="text" name="signature_date" placeholder="Text input" class="form-control datepicker" value="{{old('signature_date')}}">
							@if($errors->has('signature_date'))
	                            <small>{{$errors->first('signature_date')}}</small>
	                        @endif
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<input type="hidden" name="_token" value="{{csrf_token()}}">
				<button class="btn btn-primary"><i class="fa fa-lock"></i> Create Record</button>
			</div>
		</div>
	</form>
</div>		

@stop

@section('page-styles')
<link rel="stylesheet" type="text/css" href="{{asset('assets/frontend/plugins/form_editor/css/summernote.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/frontend/plugins/form_editor/css/summernote-bs3.css')}}">
@stop
@section('page-scripts')
<script type="text/javascript" src="{{asset('assets/frontend/plugins/form_editor/js/summernote.min.js')}}"></script>
<script type="text/javascript">
	$script.ready(['core','plugins'], function(){
		// $script(App.Scripts.plugins_dependency, 'plugins_dependency');
		$(function(){
			$(".summernote").summernote();
		});
		// (function ( jQuery ) { 

		//     jQuery(".summernote").summernote();

		// }( jQuery ));
	});

</script>
@stop