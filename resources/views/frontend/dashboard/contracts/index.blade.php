@extends('frontend._template.main')

@section('content')
<div class="innerLR">
	<h1 class="pull-left">Contracts &nbsp; <a href="{{route('dashboard.contracts.index')}}" class="btn btn-success">Add new Contract <i class="icon-add-symbol"></i> </a> </h1>
	<div class="pull-right innerT">
		<div class="btn-group">
			<a href="projects_lista10b.html?v=v1.0.0-rc1" class="btn btn-inverse"><i class="fa fa-list"></i></a>
			<a href="projects_grida10b.html?v=v1.0.0-rc1" class="btn btn-default filled"><i class="fa fa-th"></i></a>
		</div>
	</div>
	<div class="clearfix"></div>	
    
	<div class="innerTB">
        @include('frontend.page-notification')
        @foreach(range(1,10) as $i)
        <div class="widget widget-warning widget-small ">
        	<div class="widget-body padding-none">
            	<div class=" media innerAll overflow-visible margin-none">
                	<div class="pull-left innerR half hidden-xs">
                    	<i class="icon-paper-document-image fa-4x icon-faded"></i>
                	</div>
                	{{-- <div class="pull-right hidden-xs">
                    	<div class="strong text-muted-dark innerB half">Assigned to:</div>
                    		<img src="../assets/images/people/50/14.jpg" alt="photo" class="img-circle" style="width:40px;" data-toggle="tooltip" data-placement="top" data-title="Adrian Demian"/>
                    		<img src="../assets/images/people/50/16.jpg" alt="photo" class="img-circle" style="width:40px;" data-toggle="tooltip" data-placement="top" data-title="Laza Bogdan"/>
                	</div> --}}
                    <div class="media-body">
                        <h4>
                        	<a href="project_milestonesa10b.html?v=v1.0.0-rc1" class="media-heading">PHP Version of Smart [Project Title]</a>
                        </h4>
                        <p class="text-muted">Short description of the project</p>
                        <div class="clearfix"></div>
                        {{-- <div class="widget widget-none innerR innerB half margin-slim pull-left">
                            <span class="strong"> Due:</span>
                            <span> <i class="fa fa-clock-o text-muted"></i> 3 Days</span>
                        </div> --}}
                        <div class="widget widget-none innerR innerB half margin-slim pull-left">
                            <span class="strong"> Type:</span>
                            <span class="strong">
                                <a href="#"><span class="label label-primary">Infosite</span></a>
                                {{-- <a href="#"><span class="label label-warning"> CSS</span></a> --}}
                            </span>
                        </div>
                        {{-- <div class="widget widget-none innerR innerB half margin-slim pull-left">
                            <span class="pull-left strong">Progress:</span>
                            <div class="progress progress-mini set-width">
                                <div class="progress-bar progress-bar-primary" style="width: 50%;"></div>
                            </div>
                        </div> --}}
                    </div>
            	</div>
        	</div>
    	</div>
        @endforeach        
   	</div>
    <ul class="pagination pagination-centered margin-none">
	<li class="disabled"><a href="#">&laquo;</a></li>
	<li class="active"><a href="#">1</a></li>
	<li><a href="#">2</a></li>
	<li><a href="#">3</a></li>
	<li><a href="#">4</a></li>
	<li><a href="#">5</a></li>
	<li><a href="#">&raquo;</a></li>
</ul>


</div>
@stop