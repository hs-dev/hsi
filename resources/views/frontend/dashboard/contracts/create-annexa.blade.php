@extends('frontend._template.main')

@section('content')
<h1 class="pull-left">
	Create Contract - Statement of Work
</h1>
<div class="clearfix"></div>
<div class="innerLR">
	@include('frontend.page-notification')
	<form action="" method="POST">
		<div class="row">
			<div class="col-md-4">
				<div class="row">
					<div class="col-md-12">
						<div class="widget widget-heading-simple widget-body-white">
							<!-- Widget heading -->
							<div class="widget-head">
								<h4 class="heading">Project Name</h4>
							</div>
							<!-- // Widget heading END -->
							

							<div class="widget-body">
								<select name="project_name" id="" class="form-control">
									<option value="">Select Type</option>
									<option value="Webapp" {{old('project_name')=='Webapp'?'selected=selected':NULL}}>Webapp</option>
									<option value="Logo" {{old('project_name')=='Logo'?'selected=selected':NULL}}>Logo</option>
									<option value="E-commerce" {{old('project_name')=='E-commerce'?'selected=selected':NULL}}>E-commerce</option>
								</select>
								<label id="title-error" class="error" for="title">{{$errors->first('project_name')}}</label>
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="widget widget-heading-simple widget-body-white">
							<!-- Widget heading -->
							<div class="widget-head">
								<h4 class="heading">Technologies used</h4>
							</div>
							<!-- // Widget heading END -->
							
							<div class="widget-body">
								<input name="technologiesused" data-role="tagsinput" type="text" placeholder="Text input" id="" class="form-control tagsinput input-block" value="{{old('technologiesused')}}">
								<label id="title-error" class="error" for="title">{{$errors->first('technologiesused')}}</label>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="col-md-8">
				<div class="widget widget-heading-simple widget-body-white">
					<!-- Widget heading -->
					<div class="widget-head">
						<h4 class="heading">Features</h4>
					</div>
					<!-- // Widget heading END -->
					
					<div class="widget-body">
						<textarea name="features" id="text-editor1" class="summernote col-md-12 form-control" rows="5">{{old('')}}</textarea>
						<label id="title-error" class="error" for="title">{{$errors->first('features')}}</label>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4"></div>
			<div class="col-md-8">
				<div class="widget widget-heading-simple widget-body-white">
					<!-- Widget heading -->
					<div class="widget-head">
						<h4 class="heading">Wireframes</h4>
					</div>
					<!-- // Widget heading END -->
					
					<div class="widget-body">
						<textarea name="wireframes"id="text-editor1" class="summernote col-md-12 form-control" rows="5">{{old('wireframes')}}</textarea>
						<label id="title-error" class="error" for="title">{{$errors->first('wireframes')}}</label>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-4"></div>
			<div class="col-md-8">
				<div class="widget widget-heading-simple widget-body-white">
					<!-- Widget heading -->
					<div class="widget-head">
						<h4 class="heading">Project requirements</h4>
					</div>
					<!-- // Widget heading END -->
					
					<div class="widget-body">
						<textarea  name="requirements" id="text-editor1" class="summernote col-md-12 form-control" rows="5">{{old('requirements')}}</textarea>
						<label id="title-error" class="error" for="title">{{$errors->first('requirements')}}</label>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<input type="hidden" name="_token" value="{{csrf_token()}}">
				<button class="btn btn-primary btn-block"><i class="fa fa-lock"></i> Create Record</button>
			</div>
		</div>
	</form>
</div>		

@stop

@section('page-scripts')
<script type="text/javascript">
	$script.ready(['core','plugins'], function(){
		// $script(App.Scripts.plugins_dependency, 'plugins_dependency');
		$(function(){
			$(".summernote").summernote({height : "300px"});

			// $(".tagsinput").tagsinput();
		});
		// (function ( jQuery ) { 

		//     jQuery(".summernote").summernote();

		// }( jQuery ));
	});

</script>
@stop