@extends('frontend._template.main')

@section('content')
<div class="innerLR">
    <div class="innerT">
        {{-- <div class="pull-right">
            <a class="btn btn-inverse"><i class="fa fa-envelope"></i> Send Message</a>
            <a class="btn btn-default filled"><i class="fa fa-download"></i></a>
        </div> --}}
        <div class="media">

            <img src="{{asset('assets/frontend/images/people/80/22.jpg')}}" class="pull-left thumbnail" alt="Profile" />

            <div class="media-body innerAll half">
                <h3>
                    <a href="#" class="text-regular ">Adrian Demian</a> <span class="lead text-muted">@short-url</span>
                </h3>

                <a class="btn btn-default filled text-muted-dark" href="#">Logout <i class="fa fa-sign-out fa-fw"></i></a>
            </div>
        </div>
    </div>

    <div class="widget widget-tabs-content widget-tabs-responsive widget-none">

        <!-- Tabs Heading -->
<div class="widget-head bg-background text-left">
    <ul>
        <li>
            <a class="glyphicons lock" href="{{route('dashboard.settings.index')}}">
                <i></i>Account Details
            </a>
        </li>
        <li class="active">
            <a class="glyphicons user" href="{{route('dashboard.settings.info')}}">
                <i></i>User Information
            </a>
        </li>
        <li>
            <a class="glyphicons share_alt" href="#TBD">
                <i></i>History Log
            </a>
        </li>
        {{-- <li>
            <a class="glyphicons clock" href="user/projectsa10b.html?v=v1.0.0-rc1">
                <i></i>Projects &amp; Tasks
            </a>
        </li>
        <li>
            <a class="glyphicons envelope" href="user/messagesa10b.html?v=v1.0.0-rc1">
                <i></i>Messages
            </a>
        </li>
        <li>
            <a class="glyphicons share_alt" href="user/historya10b.html?v=v1.0.0-rc1">
                <i></i>History Log
            </a>
        </li> --}}
    </ul>
</div>
<!-- // Tabs Heading END -->

<div class="widget-body">

    <div class="row innerAll inner-2x">
        <div class="col-sm-6">
            <form class="">
                <div class="form-group">
                    <label for="fullname">Firstname :</label>
                    <input class="form-control" name="name" type="text" value="@username" id="fullname">
                </div>

                <div class="form-group">
                    <label for="email">Lastname:</label>
                    <input class="form-control" name="email" type="text" value="contact@mosaicpro.biz" id="email">
                </div>

                <div class="form-group">
                    <label for="password">Password:</label>
                    <input class="form-control" name="password" type="password" value="password" id="password">
                </div>

                <div class="form-group">
                    <label for="password_confirmation">Password Confirmation:</label>
                    <input class="form-control" name="password_confirmation" type="password" value="" id="password_confirmation">
                </div>

                {{-- <div class="checkbox">
                    <label for="notify">
                        <input id="notify" checked="checked" name="notify" type="checkbox">
                        Email me with updates about current projects.
                    </label>
                </div> --}}

                <div class="separator"></div>
                <div class="form-group">
                    <input class="btn btn-primary" type="submit" value="Update Profile"></div>
            </form>
        </div>
        <div class="col-sm-6">
            <div class="well">
                <h4>Additional Information</h4>
                <div class="separator"></div>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A alias aspernatur assumenda at commodi dolorem eos eum exercitationem facere ipsum mollitia nemo perspiciatis quaerat qui quidem, quod repellat repudiandae sed veritatis voluptate? Eos necessitatibus non rem repudiandae similique vero voluptatem?</p>
            </div>
        </div>
    </div>

</div>
    </div>

</div>
@stop