@if(Session::has('notification-status'))
<div class="row">
	@if(Session::get('notification-status') == "failed")
	<div class="col-md-12">
		<div class="alert alert-danger fade in">
	        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	        {!! Session::get('notification-msg') !!}
	      </div>
	</div>
	@endif

	@if(Session::get('notification-status') == "success")
	<div class="col-md-12">
		<div class="alert alert-success fade in">
	        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	        {!! Session::get('notification-msg') !!}
	      </div>
	</div>
	@endif

	@if(Session::get('notification-status') == "warning")
	<div class="col-md-12">
		<div class="alert alert-warning fade in">
	        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	        {!! Session::get('notification-msg') !!}
	      </div>
	</div>
	@endif

	@if(Session::get('notification-status') == "info")
	<div class="col-md-12">
		<div class="alert alert-info fade in">
	        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	        {!! Session::get('notification-msg') !!}
	      </div>
	</div>
	@endif
</div>
@endif