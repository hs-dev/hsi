@extends('frontend._template.auth')

@section('content')
<div class="row innerT inner-2x">
    <div class="col-md-4 col-md-offset-4 innerT inner-2x">
        <div class="innerT inner-2x">
            <div class="widget innerLR innerB margin-none">
                <h3 class="innerTB text-center">Account Access</h3>
                <div class="lock-container">
                	<form action="" method="POST">
                		{!!Form::token()!!}
	                    <div class=" text-center">
	                        <i class="fa fa-fw fa-lock fa-5x"></i>
	                        <div class="innerAll">
	                            <input class="form-control text-center bg-gray" type="text" placeholder="Username"/>
	                            <div class="innerB half"></div>
	                            <input class="form-control text-center bg-gray" type="password" placeholder="Enter Password"/>
	                            <div class="checkbox"> 
                        			<label class="checkbox-custom"> 
                        				<input type="checkbox" name="checkbox" checked="checked">
                        				<i class="fa fa-fw fa-square-o"></i> Remember Me?
                        			</label> 
                        		</div>
	                        </div>
	                        <div class="innerT half">
	                            <button type="submit" class="btn btn-primary"><i class="fa fa-fw fa-key"></i> Login </button>
	                        </div>
	                    </div>
                    </form>
                </div>
            </div>
            <div class="text-right innerT half">
                Don't have an account? <a href="#" class=" strong margin-none">Register Now!</a>
            </div>
            <div class="text-right innerT half">
                 <a href="#" class=" strong margin-none">Forgot your password?</a>
            </div>
        </div>
    </div>
</div>
@stop