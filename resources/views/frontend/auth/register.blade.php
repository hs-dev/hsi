@extends('frontend._template.auth')

@section('content')
<div class="row innerT inner-2x">
    <div class="col-md-4 col-md-offset-4 innerT inner-2x">
        <div class="innerT inner-2x">
            <div class="widget  margin-none">
            <h3 class="bg-white border-bottom innerAll  text-primary">Sign Up for Free!</h3>
            <form class="innerAll"  autocomplete="off">
            	{!!Form::token()!!}
                <div class="form-group">
                    <label for="firstname">First Name:</label>
                    <input class="form-control" name="firstname" type="text" id="firstname">
                </div>
                <div class="form-group">
                    <label for="lastname">Last Name:</label>
                    <input class="form-control" name="lastname" type="text" id="lastname">
                </div>
                <div class="form-group">
                    <label for="email">Email:</label>
                    <input class="form-control" name="email" type="email" id="email">
                </div>
                <div class="form-group">
                    <label for="password">Password:</label>
                    <input class="form-control" name="password" type="password" value="" id="password">
                </div>
                <div class="text-center innerT">
                    <p><small>*By proceeding you agree to the following <a href="#">terms and conditions</a>.</small></p>
                    <button type="submit"  class="btn btn-inverse">Submit Details</button>
                </div>
            </form>
        	</div>
        	<div class="text-right innerT half">
        	    Have Account Already? <a href="{{route('frontend.login')}}" class=" strong margin-none">Login</a>
        	</div>
        </div>
    </div>
</div>
@stop