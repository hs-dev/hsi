<?php

/*
|
|--------------------------------------------------------------------------
| Backend Routes
|--------------------------------------------------------------------------
|
*/


$router->group(['namespace' => "Backend", 'prefix' => "admin"],function(){
	$this->get('/',['as' => "backend.index",'uses' => "DashboardController@index"]);

	$this->group(['prefix' => "blogs"],function(){
		$this->get('/',['as' => "backend.blogs.index",'uses' => "BlogController@index"]);
		$this->get('create',['as' => "backend.blogs.create",'uses' => "BlogController@create"]);
	});
});