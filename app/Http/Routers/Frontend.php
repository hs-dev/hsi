<?php

/*
|
|--------------------------------------------------------------------------
| Frontend Routes
|--------------------------------------------------------------------------
|
*/

$router->group(['namespace' => "Frontend", 'prefix' => ""],function(){
	$this->get("login",['as' => "frontend.login", 'uses' => "AuthController@login"]);
	$this->post("login",['uses' => "AuthController@authenticate"]);

	$this->get("register",['as' => "frontend.register",'uses' => "AuthController@register"]);
	$this->post("register",['uses' => "AuthController@store"]);

	$this->get('logout',['as' => "frontend.logout",'uses' => "AuthController@destroy"]);

	//dashboard
	$this->group(['prefix' => ""],function(){
		$this->get('/',['as' => "dashboard.index",'uses' => "DashboardController@index"]);

		$this->group(['prefix' => "contracts"],function(){
			$this->get('/',['as' => "dashboard.contracts.index",'uses' => "ContractController@index"]);
			$this->get('create-moa',['as' => "dashboard.contracts.create-moa",'uses' => "ContractController@create_moa"]);
			$this->post('create-moa',['uses' => "ContractController@store_moa"]);

			$this->get('create-annexa',['as' => "dashboard.contracts.create-annexa",'uses' => "ContractController@create_annexa"]);
			$this->post('create-annexa',['uses' => "ContractController@store_annexa"]);

			$this->get('create-annexb',['as' => "dashboard.contracts.create-annexb",'uses' => "ContractController@create_annexb"]);
			$this->post('create-annexb',['uses' => "ContractController@store_annexb"]);

			$this->get('create-annexc',['as' => "dashboard.contracts.create-annexc",'uses' => "ContractController@create_annexc"]);
			$this->post('create-annexc',['uses' => "ContractController@store_annexc"]);
		});

		$this->group(['prefix' => "settings"],function(){
			$this->get('/',['as' => "dashboard.settings.index",'uses' => "SettingController@index"]);
			$this->get('info',['as' => "dashboard.settings.info",'uses' => "SettingController@info"]);
		});
	});
});