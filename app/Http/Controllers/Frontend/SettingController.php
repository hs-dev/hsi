<?php namespace App\Http\Controllers\Frontend;

use Illuminate\Contracts\Auth\Guard;
use App\Services\Helper;
use Session,Input;

class SettingController extends Controller{
	
	protected $auth;
	protected $response;
	public function __construct(Guard $auth){
		$this->auth = $auth;
		parent::__construct();
		 $this->response['routes']= parent::get_frontend_routes();
	}

	public function index(){
		$this->response['page_title'] = "Account Setting";

		return view('frontend.dashboard.settings.index',$this->response);
	}

	public function info(){
		$this->response['page_title'] = "User Information";

		return view('frontend.dashboard.settings.info',$this->response);
	}
}

