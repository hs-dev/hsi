<?php namespace App\Http\Controllers\Frontend;

use Illuminate\Contracts\Auth\Guard;
use Session,Input;
use App\Hsi\Models\Contract;
use App\Hsi\Models\ContractSow;
use App\Hsi\Requests\SowRequests;
use App\Hsi\Requests\ContractRequest;

class ContractController extends Controller{
	
	protected $auth;
	protected $response;
	public function __construct(Guard $auth){
		$this->auth = $auth;
		parent::__construct();
		 $this->response['routes']= parent::get_frontend_routes();
	}

	public function index(){
		$this->response['page_title'] = "Dashboard";
		$this->response['contracts'] = "";
		return view('frontend.dashboard.contracts.index',$this->response);
	}

	public function create_moa(){
		$this->response['page_title'] = "Create Contract - MOA";

		return view('frontend.dashboard.contracts.create-moa',$this->response);
	}

	public function store_moa(ContractRequest $request){
		$contract = new Contract;
        /*$contract->project_name = $request->get('project_name');
        $contract->project_type = $request->get('project_type');
        $contract->client_name = $request->get('client_name');
        $contract->client_representative = $request->get('client_representative');
        $contract->client_representative_position = $request->get('client_representative_position');*/
        $contract->fill($request->all());
        $contract->signature_date = $request->get('signature_date');
        $contact->user_id = 1;
        $contract->save();
        Session::flash('notification-status','success');
        Session::flash('notification-msg','New contract saved successfully.');
        return redirect()->route('dashboard.contracts.create-moa');

	}

	public function create_annexa(){
		$this->response['page_title'] = "Annex A";

		return view('frontend.dashboard.contracts.create-annexa',$this->response);
	}

	public function store_annexa(SowRequests $request){
		$c_sow = new ContractSow;
		/*$c_sow->contract_id=$request->get('project_name');
		$c_sow->features=$request->get('features');
		$c_sow->wireframe=$request->get('wireframes');
		$c_sow->technology=$request->get('technologiesused');
		$c_sow->project_requirement=$request->get('requirements');*/
		$c_sow->fill($request->all());

		$c_sow->save();
	}	

}

