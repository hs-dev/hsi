<?php namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller as MainController;

class Controller extends MainController {

	protected $data;
	public function __construct(){
		self::set_frontend_routes();
	}

	public function set_frontend_routes(){
		$this->data['routes'] = [
			'dashboard' => ['dashboard.index'],
			'contracts' => ['dashboard.contracts.index',
							'dashboard.contracts.create-moa',
							'dashboard.contracts.create-annexa',
							'dashboard.contracts.create-annexb',
							'dashboard.contracts.create-annexc'],

		];

	}

	public function get_frontend_routes(){
		return $this->data['routes'];
	}
}

		
  