<?php namespace App\Http\Controllers\Frontend;

use Illuminate\Contracts\Auth\Guard;
use App\Services\Helper;
use Session,Input;

class DashboardController extends Controller{
	
	protected $auth;
	protected $response;
	public function __construct(Guard $auth){
		$this->auth = $auth;
		parent::__construct();
		 $this->response['routes']= parent::get_frontend_routes();
	}

	public function index(){
		$this->response['page_title'] = "Dashboard";

		return view('frontend.dashboard.index',$this->response);
	}
}

