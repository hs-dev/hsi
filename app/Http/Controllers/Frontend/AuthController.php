<?php namespace App\Http\Controllers\Frontend;

use Illuminate\Contracts\Auth\Guard;
use Session,Input;

class AuthController extends Controller{
	
	protected $auth;
	protected $response;
	public function __construct(Guard $auth){
		$this->auth = $auth;

	}

	public function login(){
		$this->response['page_title'] = "Login";
		return view('frontend.auth.login',$this->response);
	}

	public function authenticate(){

	}

	public function register(){
		$this->response['page_title'] = "Register";
		return view('frontend.auth.register',$this->response);
	}

	public function store(){

	}

	public function destroy(){
		$this->auth->logout();
		// $data['page_title'] = "Login";
		return redirect()->route('frontend.login');
	}
}