<?php namespace App\Http\Controllers\Backend;

class DashboardController extends Controller {

	protected $response;
	public function __construct(){
		$this->response['title'] = "Dashboard";
		$this->response['sub_title'] = "";
	}

	public function index(){
		return view('backend.index',$this->response);
	}
}