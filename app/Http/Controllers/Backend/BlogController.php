<?php namespace App\Http\Controllers\Backend;

class BlogController extends Controller {

	protected $response;
	public function __construct(){
		$this->response['title'] = "Blog";
		$this->response['sub_title'] = "";
	}

	public function index(){
		return view('backend.blogs.index',$this->response);
	}

	public function create(){
		$this->response['sub_title'] = "New Blog";
		return view('backend.blogs.create',$this->response);
	}
}