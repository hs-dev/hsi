<?php namespace App\Hsi\Requests;

use App\Http\Requests\Request;

class SowRequests extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'project_name'=>"required",
			'features'=>'required',
			'wireframes' =>'required',
			'technologiesused'=>'required',
			'requirements' =>'required',
		];
	}

	public function messages()
	{
		return [
			'project_name.required'=>"*Required",
			'features.required'=>'*Required',
			'wireframes.required' =>'*Required',
			'technologiesused.required'=>'*Required',
			'requirements.required'=>'*Required',
		];
	}


}
