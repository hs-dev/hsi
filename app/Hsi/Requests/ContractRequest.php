<?php namespace App\Hsi\Requests;

use App\Http\Requests\Request;

class ContractRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'project_name'=>"required",
            'project_type'=>"required",
            'client_name'=>'required',
            'client_representative'=>'required',
            'client_representative_position'=>'required',
            'signature_date'=>'required'
        ];
    }
    public function messages()
    {
        return [
            'project_name.required'=>"The project name is required",
            'project_type.required'=>"The project type is required",
            'client_name.required'=>"The client name is required",
            'client_representative.required'=>"The client representative is required",
            'client_representative_position.required'=>"The client representative position is required",
            'signature_date.required'=>"The signature date is required",

        ];
    }

}
