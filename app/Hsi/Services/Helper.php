<?php namespace App\Hsi\Services;

use Route;

class Helper{
    public static function is_active($routes, $class = "active"){
        return is_array($routes) ? 
                (in_array(Route::currentRouteName(),$routes) ? $class : NULL)
                : (Route::currentRouteName() == $routes ? $class : NULL);
    }    

}